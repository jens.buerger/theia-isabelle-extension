# Contribution Guide

## Development Setup

* [Visual Studio Code](https://code.visualstudio.com)

### Required Extensions

* [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
* [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
* [TypeScript TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)

### Initial Setup

* Compile the source code for the first time via `yarn`

## Development

1. Modify the Source Code
2. Compile with `yarn`
3. Start the backend via the debug view
4. Make sure everything works, if not go to 1.
5. Format all modified files, fix all linting warnings!
6. Push to the repository
