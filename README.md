# Theia Isabelle Extension
A Theia-based online IDE with Isabelle language support based on the Isabelle Language Server Implementation.

**Disclaimer: This repository only contains the proof-of-concept source code of the project. Internally this project is in a much more advanced state. However, this project should still serve you well if you want to build an online IDE for Isabelle.**

## Run the IDE Server

If you just want to play with the IDE you can use docker to build and deploy the application.

1. Run `docker run -p 3000:3000 registry.gitlab.com/jens.buerger/theia-isabelle-extension:latest`
2. Check out the [Manual Wiki Page](https://gitlab.com/jens.buerger/theia-isabelle-extension/wikis/Manual)


# Development

## Getting started

Install [nvm](https://github.com/creationix/nvm#install-script).

    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash

Install npm and node.

    nvm install 8
    nvm use 8

Install yarn.

    npm install -g yarn

## Running the browser example

    yarn rebuild:browser
    cd browser-app
    yarn start

Open http://localhost:3000 in the browser.

## Running the Electron example

    yarn rebuild:electron
    cd electron-app
    yarn start

## Developing with the browser example

Start watching of isabelle.

    cd isabelle
    yarn watch

Start watching of the browser example.

    yarn rebuild:browser
    cd browser-app
    yarn watch

Launch `Start Browser Backend` configuration from VS code.

Open http://localhost:3000 in the browser.

