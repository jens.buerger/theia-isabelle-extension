/*
 * Copyright (C) 2018 Jens Buerger

 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */
import { BaseLanguageServerContribution, IConnection } from "@theia/languages/lib/node";
import { injectable } from "inversify";
import { ISABELLE_LANGUAGE_ID, ISABELLE_LANGUAGE_NAME } from "../common";

@injectable()
export class IsabelleContribution extends BaseLanguageServerContribution {
  public readonly id = ISABELLE_LANGUAGE_ID;
  public readonly name = ISABELLE_LANGUAGE_NAME;

  public start(clientConnection: IConnection): void {
    const isabelleHome = "/Applications/Isabelle2018.app/Isabelle";
    const isabelleTool = isabelleHome + "/bin/isabelle";
    const command = isabelleTool;
    const standardArgs = ["-o", "vscode_unicode_symbols", "-o", "vscode_pide_extensions"];
    const customArgs : string[] = [];
    const args: string[] = ["vscode_server"].concat(standardArgs, customArgs);
    const serverConnection = this.createProcessStreamConnection(command, args);
    this.forward(clientConnection, serverConnection);
  }

  protected onDidFailSpawnProcess(error: Error): void {
    super.onDidFailSpawnProcess(error);
    const message = "Error starting isabelle language server.\nPlease make sure it is installed on your system.";
    console.error(message);
  }
}
