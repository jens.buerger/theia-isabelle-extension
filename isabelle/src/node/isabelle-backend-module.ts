/*
 * Copyright (C) 2018 Jens Buerger and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */
import { LanguageServerContribution } from "@theia/languages/lib/node";
import { ContainerModule } from "inversify";
import { IsabelleContribution } from "./isabelle-contribution";

export default new ContainerModule((bind) => {
  bind(LanguageServerContribution)
    .to(IsabelleContribution)
    .inSingletonScope();
});
