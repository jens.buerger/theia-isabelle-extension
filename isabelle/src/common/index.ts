/*
 * Copyright (C) 2018 Jens Buerger
 *
 */
// Isabelle language constants
export const ISABELLE_LANGUAGE_ID = "isabelle";
export const ISABELLE_LANGUAGE_NAME = "Isabelle";

// Isabelle/ML language constants
export const ISABELLE_ML_LANGUAGE_ID = "isabelle-ml";
export const ISABELLE_ML_LANGUAGE_NAME = "Isabelle/ML";
