/**
 * Generated using theia-extension-generator
 */
import { CommandContribution, MenuContribution } from "@theia/core/lib/common";
import { LanguageClientContribution } from "@theia/languages/lib/browser";
import { LanguageGrammarDefinitionContribution } from "@theia/monaco/lib/browser/textmate";
import { ContainerModule } from "inversify";
import { ActiveEditorAccess } from "./active-editor-access";
import { IsabelleClientContribution } from "./isabelle-client-contribution";
import { IsabelleCommandContribution, IsabelleMenuContribution } from "./isabelle-contribution";
import { IsabelleGrammarContribution } from "./isabelle-grammar-contribution";
import { IsabelleMLGrammarContribution } from "./isabelle-ml-grammar-contribution";

export default new ContainerModule((bind) => {
  // add contribution bindings here

  bind(CommandContribution).to(IsabelleCommandContribution);
  bind(MenuContribution).to(IsabelleMenuContribution);

  // Isabelle Grammar Modules
  bind(LanguageGrammarDefinitionContribution)
    .to(IsabelleGrammarContribution)
    .inSingletonScope();

  // Isabelle/ML Grammar Modules
  bind(LanguageGrammarDefinitionContribution)
    .to(IsabelleMLGrammarContribution)
    .inSingletonScope();

  // ActiveEditorAccess
  bind(ActiveEditorAccess).toSelf().inSingletonScope();

  // Language Server
  bind(IsabelleClientContribution)
    .toSelf()
    .inSingletonScope();
  bind(LanguageClientContribution)
    .toDynamicValue((ctx) => ctx.container.get(IsabelleClientContribution))
    .inSingletonScope();
});
