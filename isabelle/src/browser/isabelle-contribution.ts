/*
 * Copyright (C) 2018 Jens Buerger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */
import { CommonMenus } from "@theia/core/lib/browser";
import {
    CommandContribution, CommandRegistry,
    MenuContribution, MenuModelRegistry, MessageService,
} from "@theia/core/lib/common";
import { inject, injectable } from "inversify";

export const IsabelleCommand = {
    id: "Isabelle.command",
    label: "Shows a message",
};

@injectable()
export class IsabelleCommandContribution implements CommandContribution {

    constructor(
        @inject(MessageService) private readonly messageService: MessageService,
    ) { }

    public registerCommands(registry: CommandRegistry): void {
        registry.registerCommand(IsabelleCommand, {
            execute: () => this.messageService.info("Hello World From JC!"),
        });
    }
}

// tslint:disable-next-line: max-classes-per-file
@injectable()
export class IsabelleMenuContribution implements MenuContribution {
    public registerMenus(menus: MenuModelRegistry): void {
        menus.registerMenuAction(CommonMenus.EDIT_FIND, {
            commandId: IsabelleCommand.id,
            label: "Say Hello",
        });
    }
}
