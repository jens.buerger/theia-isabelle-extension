/*
 * Copyright (C) 2018 Jens Buerger and others.
 * Adapted from the Isabelle VSCode project
 *
 */
"use strict";

import { MarkedString } from "@theia/languages/lib/browser";
import { NotificationType } from "@theia/languages/lib/browser";


/* decorations */

export interface IDecorationOpts {
  range: number[];
  hover_message?: MarkedString | MarkedString[];
}

export interface IDecoration {
  uri: string;
  type: string;
  content: IDecorationOpts[];
}

export const decorationType = new NotificationType<IDecoration, void>("PIDE/decoration");

/* caret handling */

export interface ICaretUpdate {
  uri?: string;
  line?: number;
  character?: number;
  focus?: boolean;
}

export const caretUpdateType = new NotificationType<ICaretUpdate, void>("PIDE/caret_update");

/* dynamic output */

export interface IDynamicOutput {
  content: string;
}

export const dynamicOutputType = new NotificationType<IDynamicOutput, void>("PIDE/dynamic_output");

/* state */

export interface IStateOutput {
  id: number;
  content: string;
}

export const stateOutputType = new NotificationType<IStateOutput, void>("PIDE/state_output");

export interface IStateId {
  id: number;
}

export interface IAutoUpdate {
  id: number;
  enabled: boolean;
}

export const stateInitType = new NotificationType<void, void>("PIDE/state_init");
export const stateExitType = new NotificationType<IStateId, void>("PIDE/state_exit");
export const stateLocateType = new NotificationType<IStateId, void>("PIDE/state_locate");
export const stateUpdateType = new NotificationType<IStateId, void>("PIDE/state_update");
export const stateAutoUpdateType = new NotificationType<IAutoUpdate, void>("PIDE/state_auto_update");

/* preview */

export interface IPreviewRequest {
  uri: string;
  column: number;
}

export interface IPreviewResponse {
  uri: string;
  column: number;
  label: string;
  content: string;
}

export const previewRequestType = new NotificationType<IPreviewRequest, void>("PIDE/preview_request");

export const previewResponseType = new NotificationType<IPreviewResponse, void>("PIDE/preview_response");

export const includeWordType = new NotificationType<void, void>("PIDE/include_word");

export const includeWordPermanentlyType = new NotificationType<void, void>("PIDE/include_word_permanently");

export const excludeWordType = new NotificationType<void, void>("PIDE/exclude_word");

export const excludeWordPermanentlyType = new NotificationType<void, void>("PIDE/exclude_word_permanently");

export const resetWordsType = new NotificationType<void, void>("PIDE/reset_words");
