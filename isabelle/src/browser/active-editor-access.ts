/*
 * Copyright (C) 2017 TypeFox and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */

import { EditorManager, TextEditor } from "@theia/editor/lib/browser";
import { inject, injectable } from "inversify";
import { Location } from "vscode-languageserver-types";

@injectable()
export class ActiveEditorAccess {
  constructor(@inject(EditorManager) protected readonly editorManager: EditorManager) {}

  public getSelection(): Location | undefined {
    const activeEditor = this.getActiveEditor();
    if (!activeEditor) {
      return;
    }
    const range = activeEditor.selection;
    const uri = activeEditor.uri.toString();
    return { range, uri } as Location;
  }

  public getLanguageId(): string | undefined {
    const activeEditor = this.getActiveEditor();
    if (!activeEditor) {
      return;
    }
    return activeEditor.document.languageId;
  }

  public getActiveEditor(): TextEditor | undefined {
    const activeEditor = this.editorManager.currentEditor;
    if (activeEditor) {
      return activeEditor.editor;
    }
    return undefined;
  }
}
