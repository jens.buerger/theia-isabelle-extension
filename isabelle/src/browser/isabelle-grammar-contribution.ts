/*
 * Copyright (C) 2018 Jens Buerger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */
import { LanguageGrammarDefinitionContribution, TextmateRegistry } from "@theia/monaco/lib/browser/textmate";
import { injectable } from "inversify";
import { ISABELLE_LANGUAGE_ID, ISABELLE_LANGUAGE_NAME} from "../common";

@injectable()
export class IsabelleGrammarContribution implements LanguageGrammarDefinitionContribution {
  private readonly scopeName = "source.isabelle";
  private readonly config: monaco.languages.LanguageConfiguration =
    require("../../data/isabelle.tmLanguage.config.json");

  public registerTextmateLanguage(registry: TextmateRegistry) {
    monaco.languages.register({
      aliases: [ISABELLE_LANGUAGE_NAME, "isabelle"],
      extensions: [".thy"],
      id: ISABELLE_LANGUAGE_ID,
      mimetypes: ["text/x-isabelle-source", "text/x-isabelle"],
    });

    monaco.languages.setLanguageConfiguration(ISABELLE_LANGUAGE_ID, this.config);

    const isabelleGrammar = require("../../data/isabelle.tmLanguage.json");
    registry.registerTextmateGrammarScope(this.scopeName, {
      async getGrammarDefinition() {
        return {
          content: isabelleGrammar,
          format: "json",
        };
      },
    });

    registry.mapLanguageIdToTextmateGrammar(ISABELLE_LANGUAGE_ID, this.scopeName);
  }
}
