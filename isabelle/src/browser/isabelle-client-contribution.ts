/*
 * Copyright (C) 2018 Jens Buerger
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */
import {
  BaseLanguageClientContribution,
  LanguageClientFactory,
  Languages,
  ILanguageClient,
  Window,
  Workspace,
  OutputChannel,
} from "@theia/languages/lib/browser";
import {
  inject,
  injectable
} from "inversify";
import {
  ISABELLE_LANGUAGE_ID,
  ISABELLE_LANGUAGE_NAME
} from "../common";
import * as protocol from "./protocol";
import * as library from "./library";
import { ActiveEditorAccess } from "./active-editor-access";
import URI from "@theia/core/lib/common/uri";
import {  TextEditor } from "@theia/editor/lib/browser";

let lastCaretUpdate: protocol.ICaretUpdate = {}
let dynamicOutput : OutputChannel
let observedEditors: Set<TextEditor> = new Set<TextEditor>();

@injectable()
export class IsabelleClientContribution extends BaseLanguageClientContribution {
  public readonly id = ISABELLE_LANGUAGE_ID;
  public readonly name = ISABELLE_LANGUAGE_NAME;

  constructor(
    @inject(Workspace) protected readonly workspace: Workspace,
    @inject(Languages) protected readonly languages: Languages,
    @inject(Window) protected readonly window: Window,
    @inject(ActiveEditorAccess) protected readonly editorAccess: ActiveEditorAccess,
    @inject(LanguageClientFactory) protected readonly languageClientFactory: LanguageClientFactory,
  ) {
    super(workspace, languages, languageClientFactory);
  }

  protected get globPatterns() {
    return ["**/*.thy"];
  }

  public update_caret(languageClient: ILanguageClient, bla: any) {
    const editor = bla.editorAccess.getActiveEditor();
    let caretUpdate: protocol.ICaretUpdate = {};
    console.log(editor);
    if (editor) {
      const uri = editor.document.uri
      const cursor = editor.cursor
      if (library.is_file(new URI(uri)) && cursor)
        caretUpdate = {
          uri: uri.toString(),
          line: cursor.line,
          character: cursor.character
        }
    }
    if (lastCaretUpdate !== caretUpdate) {
      if (caretUpdate.uri)
        languageClient.sendNotification(protocol.caretUpdateType, caretUpdate)
      lastCaretUpdate = caretUpdate;
    }
  }

  protected onReady(languageClient: ILanguageClient): void {
      // very hacky but efficient solution -> should be improved in the future
      if (this.workspace) {
        this.workspace.onDidChangeTextDocument(_ => {
          const editor = this.editorAccess.getActiveEditor();
          const caretUpdate = this.update_caret;
          if (editor && !observedEditors.has(editor)) {
            editor.onCursorPositionChanged(_ => caretUpdate(languageClient, this));
            editor.onSelectionChanged(_ => caretUpdate(languageClient, this));
            observedEditors.add(editor);
          }
        });
        this.workspace.onDidCloseTextDocument(textDocument => {
          observedEditors.forEach(editor => {
              if (editor.uri.toString() == textDocument.uri) {
                observedEditors.delete(editor);
              }
          });
        });
      }
    this.update_caret(languageClient,this);

    const window: Window = this.window;  
    if (!!window && !!window.createOutputChannel){
      // create output window
      dynamicOutput = window.createOutputChannel("Isabelle Output");
      dynamicOutput.show(true);
      languageClient.onNotification(protocol.dynamicOutputType, 
        // TODO OPT reuse the output channel
        params => { dynamicOutput.dispose(); 
          dynamicOutput = window.createOutputChannel!("Isabelle Output");  
          dynamicOutput.show(true);
          dynamicOutput.appendLine(params.content);});
    }

    super.onReady(languageClient);
  }
}