/*
 * Copyright (C) 2018 Jens Buerger and others.
 * Adapted from the Isabelle VSCode project
 *
 */
"use strict";

import URI from "@theia/core/lib/common/uri";

export function escape_regex(s: string): string {
  return s.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&");
}

export function is_file(uri: URI): boolean {
  return uri.scheme === "file";
}

