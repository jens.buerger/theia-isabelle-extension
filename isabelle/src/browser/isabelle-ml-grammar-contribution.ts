/*
 * Copyright (C) 2018 Jens Buerger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */
import { LanguageGrammarDefinitionContribution, TextmateRegistry } from "@theia/monaco/lib/browser/textmate";
import { injectable } from "inversify";
import { ISABELLE_ML_LANGUAGE_ID, ISABELLE_ML_LANGUAGE_NAME } from "../common";

@injectable()
export class IsabelleMLGrammarContribution implements LanguageGrammarDefinitionContribution {
  private readonly scopeName = "source.isabelle-ml";
  private readonly config: monaco.languages.LanguageConfiguration =
    require("../../data/isabelle-ml.tmLanguage.config.json");

  public registerTextmateLanguage(registry: TextmateRegistry) {
    monaco.languages.register({
      aliases: [ISABELLE_ML_LANGUAGE_NAME, "isabelle"],
      extensions: [".ML", ".sml", ".sig"],
      id: ISABELLE_ML_LANGUAGE_ID,
      mimetypes: ["text/x-isabelle-ml-source", "text/x-isabelle-ml"],
    });

    monaco.languages.setLanguageConfiguration(ISABELLE_ML_LANGUAGE_ID, this.config);

    const isabelleMLGrammar = require("../../data/isabelle-ml.tmLanguage.json");
    registry.registerTextmateGrammarScope(this.scopeName, {
      async getGrammarDefinition() {
        return {
          content: isabelleMLGrammar,
          format: "json",
        };
      },
    });

    registry.mapLanguageIdToTextmateGrammar(ISABELLE_ML_LANGUAGE_ID, this.scopeName);
  }
}
