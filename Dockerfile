FROM montibelle/isabelle:2018

USER root

RUN apt-get update && apt-get install -y python build-essential git

USER isabelle

ENV NVM_DIR /home/isabelle/nvm 
ENV NODE_VERSION 8

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default \
    && npm install -g yarn 

ARG INSTALLDIR=/home/isabelle/ide-app
ADD . /rawfiles

RUN . $NVM_DIR/nvm.sh \
    && mkdir -p $INSTALLDIR \
    && cp -r /rawfiles/* $INSTALLDIR/ \
    && mkdir -p /home/isabelle/ide-workspace \
    && sed -i -e "s@isabelleHome =.*@isabelleHome = '/home/isabelle/Isabelle';@g" \
        $INSTALLDIR/isabelle/src/node/isabelle-contribution.ts \
    && . $NVM_DIR/nvm.sh \
    && cd ${INSTALLDIR} \
    && yarn

EXPOSE 3000


##RUN . $NVM_DIR/nvm.sh && 
CMD ["bash", "-c", ". $NVM_DIR/nvm.sh && cd /home/isabelle/ide-app/browser-app && yarn start --hostname 0.0.0.0"]
